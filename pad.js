function Pad(sizeX, sizeY, padSpeed, up, down) {
	this.up = up;
	this.down = down;
	this.sizeX = sizeX;
	this.sizeY = sizeY;
	this.dir = 0;
	this.max_speed = 20;
	this.decel = 0.71;

	var _this = this;

	this.setup = function (posX, posY) {
		_this.loc = new p5.Vector(posX, posY);
		_this.vel = new p5.Vector(0, 0);
		_this.acc = new p5.Vector(0, 1);
	}
	// hitbox of the pad
	this.hitbox = function() {
		if (ball.loc.y + ball.vel.y < _this.loc.y) {
			return false;
		}

		if (ball.loc.y + ball.vel.y > _this.loc.y + _this.sizeY) {
			return false;
		}
		return true;
	}

	this.hitboxTopBot = function() {
		if (ball.loc.x + ball.vel.x < _this.loc.x) {
			return false;
		}

		if (ball.loc.x + ball.vel.x > _this.loc.x + _this.sizex) {
			return false;
		}
		return true;
	}
	// change direction based on keyMap
	this.move = function(speedMod) {
		if (keyMap[_this.up]) {
			_this.dir = -1;
		}
		else if (keyMap[_this.down]) {
			_this.dir = 1;
		}
		else {
			_this.dir = 0;
		}

		// Pad hits top or bottem. No movement, reset velocity
		if (
			(_this.loc.y <= 0 && _this.dir <= 0) ||
			(_this.loc.y + _this.sizeY >= height && _this.dir >= 0)
		)
		{
			_this.vel.mult(0);
			return;
		}

		// No key is pressed. Negative acceleration
    if(_this.dir == 0) {
			_this.vel.mult(_this.decel);
		}

		// Increase velocity by acceleration
		else {
			_this.vel.add(_this.acc.copy().mult(_this.dir));
			_this.vel.limit(_this.max_speed + speedMod);
		}

		_this.loc.add(_this.vel);
	}
	// draw the pad
	this.drawPad = function(color, speedMod) {
		_this.move(speedMod);
		fill(color);
		rectMode(CORNER);
		rect(_this.loc.x, _this.loc.y, _this.sizeX, _this.sizeY);

		// draw round edges
		//stroke(255);
		//ellipse(_this.loc.x+_this.sizeX/2+0.5, _this.loc.y, _this.sizeX, _this.sizeX);
		//ellipse(_this.loc.x+_this.sizeX/2+0.5, _this.loc.y+_this.sizeY, _this.sizeX, _this.sizeX);
	}
}
