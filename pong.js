var pad1KeyUp=87;
var pad1KeyDown=83;
var pad2KeyUp=38;
var pad2KeyDown=40;
var keyMap = {};
var scoreLeft = 0;
var scoreRight = 0;

var canHeight = window.innerHeight - 20;
var canWidth = canHeight*1.5;
var scored = true;
var maxScore = 5;
var ballStartLeft = true;

var ballStartX = canWidth/2;
var ballStartY = canHeight/2;
var ballSize = canHeight/40;
var ballRadius = ballSize/2;

var nloc;                 // where the ball would land
var g;                    // how much the ball is outside x-axis
var velX = 1;
var acc = 1;
var rnd;

var randomPosNeg = [ 1, -1];

var copyVector = new p5.Vector(0, 0);

var gameState = 0; //0=Menu, 1=running, 2=settings

var buttonWidth = canWidth/5;
var buttonHeight = canHeight/12;

var black = 0;
var white = 255;

var speedMod = 0;
var velY;

function setup () {
  document.onkeyup = handleKeyEvent;
  document.onkeydown = handleKeyEvent;
  rnd = random(randomPosNeg)
  velY = rnd;

  createCanvas(canWidth, canHeight);

  menu = new Menu(canWidth, canHeight, buttonWidth, buttonHeight);
  ball = new Ball(ballSize);
  ball.setup(ballStartX, ballStartY,velX, velY);
  pad1 = new Pad(width/60, height/8, 3, pad1KeyUp, pad1KeyDown);
	pad1.setup(width/60, height/2 - (height/8)/2);
  pad2 = new Pad(width/60, height/8, 3, pad2KeyUp, pad2KeyDown);
  pad2.setup(width/60*58, height/2 - (height/8)/2);

  frameRate(120);
}

function draw() {
  if(gameState == 1)
  {
    background(black);
    pad1.drawPad(white, speedMod);
    pad2.drawPad(white, speedMod);
    ball.drawPos(white, speedMod);

    hit();
    writeScore();
    // Check if game is running or between points/games
    if (scored){
      // Reset Ball
      if(scoreLeft == 0 && scoreRight == 0)
      {
        velY = rnd;
        ballStartX = width / 2;
        ballStartY = height / 2;
        velX = rnd;
      }
      else
      {
        if (ballStartLeft){
          ballStartX = pad1.loc.x + pad1.sizeX + ballRadius;
          ballStartY = pad1.loc.y + (pad1.sizeY / 2);
          velX = 1;
          if (pad1.dir == 0){
            velY = rnd;
          } else {
            velY = pad1.dir;
          }

        } else {
          ballStartX = pad2.loc.x - ballRadius;
          ballStartY = pad2.loc.y + (pad2.sizeY / 2);
          velX = -1;
          if (pad2.dir == 0){
            velY = rnd;
          } else {
            velY = pad2.dir;
          }

        }
      }


      ball.setup(ballStartX, ballStartY, velX, velY);
      // If a Player has won show message
      if(scoreLeft >= maxScore){
        background(0);
        writeNew(1);
      }else if(scoreRight >= maxScore){
        background(0);
        writeNew(0);
      }else{
      // If no one has won yet show ready message
      writeReady();
      }
    }

  }
  else if(gameState == 0)
  {
    menu.mainMenu();
  }
}

function mousePressed() {

  if(gameState == 0) // Mouse checks if the Game is in the Menu
  {
    if(mouseX > width/2-(buttonWidth/2)
    && mouseX < width/2+(buttonWidth/2)
    && mouseY > height/1.75-(buttonHeight/2)
    && mouseY < height/1.75+(buttonHeight/2))
    {
      gameState = 1;
    }
  }

  return false;
}

function keyPressed(){
  // Check for SPACE press
  if(keyCode === 32)
  {
    // Continue Game
    if(gameState == 1)
    {
      scored = false;
    }
    else if(gameState == 0)
    {
      gameState = 1;
    }
    // If the game was won and restartet reset score
    if(scoreLeft >= maxScore ||  scoreRight >= maxScore)
    {
      gameState = 0;
      scoreRight = 0;
      scoreLeft = 0;
      speedMod = 0;
      scored = true;
      pad1.setup(width/60, height/2 - (height/8)/2);
      pad2.setup(width/60*58, height/2 - (height/8)/2);
    }
  }
}

function writeNew(p) {
  // Text after winning
  background(255);
  textSize(canHeight/10);
  fill(black);
  if(p == 1){
    text("PLAYER 1 WINS!", width/2, height/2-canHeight/20);
  }else{
    text("PLAYER 2 WINS!", width/2, height/2-canHeight/20);
  }
  textSize(canHeight/30);
  text("PRESS SPACE", width/2, height/2+canHeight/20);

  textAlign(CENTER, TOP);
  textSize(canHeight/10);
  fill(black);
  text(scoreLeft + " : " + scoreRight, width/2, canHeight/10);
}

function writeScore() {
  // Scoreboard
	textAlign(CENTER, TOP);
	textSize(canHeight/10);
	fill(white);
	text(scoreLeft + " : " + scoreRight, width/2, canHeight/10);
}


function writeReady() {
  // Text after a point was made
  background(255);
  textSize(canHeight/10);
  fill(black);
  text("READY", width/2, height/2-canHeight/20);
  textSize(canHeight/30);
  fill(black);
  text("PRESS SPACE", width/2, height/2+canHeight/20);

  textAlign(CENTER, TOP);
  textSize(canHeight/10);
  fill(black);
  text(scoreLeft + " : " + scoreRight, width/2, canHeight/10);

  pad1.drawPad(black, speedMod);
  pad2.drawPad(black, speedMod);

  if(scoreLeft != 0 || scoreRight != 0)
  {
    ball.drawPos(black, speedMod);
  }
  speedMod = 0;
}

function hit() {

  if(ball.vel.x < 0)
  {
    // left wall collision
    if (ball.loc.x < 0 + ballRadius) {
  		scoreRight += 1;
      scored = true;
      ballStartLeft = false;
    }

    // Pad1 Collision
    if ((ball.loc.x + ball.vel.x) <= pad1.loc.x + pad1.sizeX  + ballRadius ) {
      if(pad1.hitbox()){
        // copy current velocity in new vector
        copyVector = ball.vel.copy();
        // location the ball would land behind the pad
        nloc = (ball.loc.x + ball.vel.x);
        // height difference between pad size and nloc
        g = nloc + pad1.loc.x + pad1.sizeX + ballRadius;
        // calculate direction
        copyVector.normalize().mult(g);
        ball.vel.x = copyVector.x;
        // effet
        ball.vel.add(pad1.vel.copy().mult(pad1.dir));
        // change direction and add acceleration
        ball.acc.x *= -1;
        ball.vel.x *= -1;
        // increase speed limit
        speedMod = speedMod + 0.5;
      }
      else if ((ball.loc.y + ball.vel.y) >= pad1.loc.y - ballRadius && (ball.loc.y + ball.vel.y) <= pad1.loc.y + pad1.sizeY + ballRadius){
        if(pad1.hitboxTopBot()){
          ball.acc.y *= -1;
          ball.vel.y *= -1;
        }
      }
    }
  }
  else if(ball.vel.x > 0)
  {
  	// right wall collision
  	if (ball.loc.x > width - ballRadius) {
  		scoreLeft += 1;
      scored = true;
      ballStartLeft = true;
    }

    // Pad2 Collision
  	if ((ball.loc.x + ball.vel.x) >= pad2.loc.x - ballRadius) {
      if(pad2.hitbox()){
        // copy current velocity in new vector
        copyVector = ball.vel.copy();
        // location the ball would land behind the pad
        nloc = (ball.loc.x + ball.vel.x);
        // height difference between pad size and nloc
        g = nloc - pad2.loc.x - ballRadius;
        // calculate direction
        copyVector.normalize().mult(g);
        ball.vel.x = copyVector.x;
        // effet
        ball.vel.add(pad2.vel.copy().mult(pad2.dir));
        // change direction and add acceleration
        ball.acc.x *= -1;
        // increase speed limit
        speedMod = speedMod + 0.5;
      }
      else if ((ball.loc.y + ball.vel.y) >= pad2.loc.y - ballRadius && (ball.loc.y + ball.vel.y) <= pad2.loc.y + pad2.sizeY + ballRadius){
        if(pad2.hitboxTopBot()){
          ball.acc.y *= -1;
          ball.vel.y *= -1;
        }
      }
    }
  }

  if(ball.vel.y < 0)
  {
    // top wall collision (checks if the ball would go through the wall)
    if ((ball.loc.y + ball.vel.y) < 0 + ballRadius) {
      // copy current velocity in new vector
      copyVector = ball.vel.copy();
      // location the ball would land behind the wall
      nloc = (ball.loc.y + ball.vel.y);
      // height difference between canvas height and loc the ball would be
      g = nloc - ballRadius;
      // calculate and change direction
      copyVector.normalize().mult(g);
      ball.vel.y = copyVector.y;
      ball.acc.y *= -1;
    }
  }
  else if (ball.vel.y > 0)
  {
    // bottom wall collision (checks if the ball would go through the wall)
    if ((ball.loc.y + ball.vel.y) > height  - ballRadius) {
      // copy current velocity in new vector
      copyVector = ball.vel.copy();
      // location the ball would land behind the wall
      nloc = (ball.loc.y + ball.vel.y);
      // height difference between canvas height and loc the ball would be
      g = nloc  - height + ballRadius;
      // calculate and change direction
      copyVector.normalize().mult(g);
      ball.vel.y = copyVector.y;
      ball.vel.y *= -1;
      ball.acc.y *= -1;
    }
  }
}

function handleKeyEvent(e) {
    e = e || event; // to deal with IE
    keyMap[e.keyCode] = e.type == 'keydown';
}
