function Menu(width, height, buttonWidth, buttonHeight){

  var buttonRectStrokeWeight = height/150;
  var buttonTextStrokeWeight = 0;

  var textSizeTitle = height/5;

  var textSizeButton = height/20;
  var TextSizeFoot = height/50;

  var textPosTitleX = width/2;
  var textPosTitleY = height/4;

  var textPosFoot1X = width/80;
  var textPosFoot2X = width-(width/80);
  var textPosFootY = height-TextSizeFoot*2;

  var KeyWidthHeight = buttonHeight/1.5;
  var KeyPosXp1 = width/2-width/3;
  var KeyPosYp1 = height/2;
  var KeyPosXp2 = width/2+width/3;
  var KeyPosYp2 = height/2;
  var KeyRectPosY = height/1.7;
  var textSizeSubTitle = height/40;
  var keyStrokeWeight = width/600

  var white = 255;
  var black = 0;

  var up = String.fromCharCode(203);

  this.mainMenu = function () {

    background(black);
    stroke(white);
    strokeWeight(buttonTextStrokeWeight);
    fill(white);
    textAlign(CENTER, CENTER);
    textSize(textSizeTitle);
    text("PRONG",textPosTitleX, textPosTitleY);

    if(mouseX > (width/2-(buttonWidth/2))
    && mouseX < ((width/2+(buttonWidth/2)))
    && mouseY > height/1.75-(buttonHeight/2)
    && mouseY < (height/1.75+(buttonHeight/2)))
    {
      stroke(black);
      strokeWeight(buttonRectStrokeWeight);
      fill(white);
      rectMode(CORNER);
      rect(width/2-(buttonWidth/2), height/1.75-(buttonHeight/2), buttonWidth, buttonHeight);

      stroke(black);
      strokeWeight(buttonTextStrokeWeight);
      fill(black);
      textAlign(CENTER, CENTER);
      textSize(textSizeButton);
      text("PLAY",width/2-(buttonWidth/2)+buttonRectStrokeWeight, height/1.75-(buttonHeight/2)+buttonRectStrokeWeight, buttonWidth, buttonHeight);
    }
    else
    {
      stroke(white);
      strokeWeight(buttonRectStrokeWeight);
      fill(black);
      rectMode(CORNER);
      rect(width/2-(buttonWidth/2), height/1.75-(buttonHeight/2), buttonWidth, buttonHeight);

      stroke(white);
      strokeWeight(buttonTextStrokeWeight);
      fill(white);
      textAlign(CENTER, CENTER);
      textSize(textSizeButton);
      text("PLAY",
            width/2-(buttonWidth/2)+buttonRectStrokeWeight,
            height/1.75-(buttonHeight/2)+buttonRectStrokeWeight,
            buttonWidth,
            buttonHeight);
    }

    fill(white);
    textAlign(CENTER, CENTER);
    textSize(textSizeSubTitle);
    strokeWeight(0);
    text("PLAYER 1", KeyPosXp1, KeyPosYp1-KeyWidthHeight/2);
    text("CONTROLS", KeyPosXp1, KeyPosYp1);
    text("PLAYER 2", KeyPosXp2, KeyPosYp2-KeyWidthHeight/2);
    text("CONTROLS", KeyPosXp2, KeyPosYp2);

    stroke(white);
    strokeWeight(keyStrokeWeight);
    fill(black);
    rectMode(CENTER);
    rect(KeyPosXp1, KeyRectPosY-(KeyWidthHeight/2), KeyWidthHeight, KeyWidthHeight);

    rect(KeyPosXp1, KeyRectPosY+(KeyWidthHeight), KeyWidthHeight, KeyWidthHeight);

    rect(KeyPosXp2, KeyRectPosY-(KeyWidthHeight/2), KeyWidthHeight, KeyWidthHeight);

    rect(KeyPosXp2, KeyRectPosY+(KeyWidthHeight), KeyWidthHeight, KeyWidthHeight);

    fill(white);

    textAlign(CENTER, CENTER);
    strokeWeight(0);
    textSize(textSizeSubTitle);
    textStyle(NORMAL);
    text("W", KeyPosXp1 + keyStrokeWeight, KeyRectPosY-(KeyWidthHeight/2) + keyStrokeWeight, KeyWidthHeight, KeyWidthHeight);

    fill(white);
    textAlign(CENTER, CENTER);
    textSize(textSizeSubTitle);
    text("S", KeyPosXp1 + keyStrokeWeight, KeyRectPosY+(KeyWidthHeight) + keyStrokeWeight, KeyWidthHeight, KeyWidthHeight);

    fill(white);
    textAlign(CENTER, CENTER);
    textSize(textSizeSubTitle);
    text("⇧", KeyPosXp2 + keyStrokeWeight, KeyRectPosY-(KeyWidthHeight/2) + keyStrokeWeight, KeyWidthHeight, KeyWidthHeight);

    fill(white);
    textAlign(CENTER, CENTER);
    textSize(textSizeSubTitle);
    text("⇩", KeyPosXp2 + keyStrokeWeight, KeyRectPosY+(KeyWidthHeight) + keyStrokeWeight, KeyWidthHeight, KeyWidthHeight);


    stroke(white);
    strokeWeight(buttonTextStrokeWeight);
    fill(white);

    textAlign(RIGHT);
    textSize(TextSizeFoot);
    text("by Andrea & Andrea",textPosFoot2X, textPosFootY + TextSizeFoot);

  }
}
