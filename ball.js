function Ball(ballSize) {

  var _this = this;
  var random = Math.random();


  this.setup = function(posX, posY, velX, velY) {
    _this.loc = new p5.Vector(posX, posY);
    _this.vel = new p5.Vector(random * velX, random * velY);
    _this.acc = new p5.Vector(0.2 * velX, 0.2 * velY);

  }
	// draw position of the ball
  this.drawPos = function(color, speedMod) {
    this.vel.add(_this.acc);
    //acc.mult(0.95);
    //_this.vel.mult(0.90);
    this.vel.limit(9 + speedMod);
    _this.loc.add(_this.vel);
    fill(color);
    ellipse(_this.loc.x, _this.loc.y, ballSize, ballSize);
  }
}
